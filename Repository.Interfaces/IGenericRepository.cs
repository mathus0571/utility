﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity:class
    {
        IQueryable<TEntity> GetAll();
        //IQueryable<TEntity> Asqueryable();
        //EntityEntry<TEntity> Add(TEntity entity, bool IsComit = false);
        //EntityEntry<TEntity> Update(TEntity entity, bool IsComit = false);
        //int Delete(TEntity entity, bool IsComit = false);
        //IQueryable<TEntity> FromSql(string sql);
        //IQueryable<TEntity> Database();
        //DbContext Entity();
    }
}
