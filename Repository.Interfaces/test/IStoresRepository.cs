﻿using Repository.Models.test;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interfaces.test
{
    public interface IStoresRepository:IGenericRepository<Store>
    {
    }
}
