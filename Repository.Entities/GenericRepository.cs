﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;

namespace Repository.Entities
{
    public abstract class GenericRepository<TContext, TEntity> : IGenericRepository<TEntity>
        where TEntity : class where TContext : DbContext
    {
        protected TContext Entities;
        protected DbSet<TEntity> DataSet;

        public GenericRepository(TContext contex)
        {
            Entities = contex;
            DataSet = Entities.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return DataSet;
        }
    }
}
