﻿using Repository.Interfaces.test;
using Repository.Models.test;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Entities.test
{
    public class StoresRepository:GenericRepository<testContext,Store>, IStoresRepository
    {
        public StoresRepository(testContext dbcontext) : base(dbcontext)
        {


        }
    }
}
