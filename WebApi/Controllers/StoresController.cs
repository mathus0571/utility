﻿using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces.test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    public class StoresController : Controller
    {
        private IStoresRepository _IStoresRepository;
        public StoresController(IStoresRepository _IStoresRepository)
        {
            this._IStoresRepository = _IStoresRepository;
        }
        public IActionResult Index()
        {
            var test = _IStoresRepository.GetAll();
            return View();
        }
    }
}
